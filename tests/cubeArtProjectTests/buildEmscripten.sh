set -e
set -x
rm -rf CMakeFiles || true
rm CMakeCache.txt || true
rm cmake_install.cmake || true
rm Makefile || true
source ../../../../emsdk/emsdk_env.sh

echo "Arguments: $@"

if [[ $1 == "-defaultTestRunner" ]]; then
emcmake cmake -DEMSCRIPTEN=1 -DCUBE_ART_PROJECT_DEFAULT_TEST_RUNNER_BUILD=1 .
else
emcmake cmake -DEMSCRIPTEN=1 .
fi
emmake make VERBOSE=1
