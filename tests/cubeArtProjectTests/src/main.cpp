#include <iostream>
#include <memory>
#include <CubeArtProject/CubeArtProject.h>
#include <FlameSteelCommonTraits/Path.h>
#include <FlameSteelCommonTraits/Color.h>
#include "DefaultTestRunner.h"
#include <FlameSteelCore/Unused.h>

using namespace std;
using namespace CubeArtProject;
using namespace FlameSteelCommonTraits;
using namespace CubeArtProjectTests;

int main(int argc, char *argv[]) {
    try
    {
        auto testName = string("Cube build remove test");

        unused(FSGL_OBJECT_ID);
        cout << testName << " started; argc: " << argc << " argv:" << argv << endl;

        for (auto i = 0; i < argc; ++i) {
            auto argument = string(argv[i]);
            cout << "ARGUMENT:" << argument << endl;
        }

        auto controller = make_shared<CubeArtProject::MainGameController>();

        if (argc > 1) {
            controller->startedSerializedScene = make_shared<string>(argv[1]);
        }

        controller->start();

#if CUBE_ART_PROJECT_DEFAULT_TEST_RUNNER_BUILD
        {
            auto testRunner = make_shared<DefaultTestRunner>();
            testRunner->inputController = controller->inputController;
            testRunner->mainGameController = controller;
            controller->testRunner = testRunner;
        }
#else
        for (auto i = 0; i < argc; ++i) {
            auto argument = string(argv[i]);
            cout << "ARGUMENT:" << argument << endl;
            if (argument == string("-defaultTestRunner")) {
                cout << "ADDDED TEST RUNNER" << endl;
                auto testRunner = make_shared<DefaultTestRunner>();
                testRunner->inputController = controller->inputController;
                testRunner->mainGameController = controller;
                controller->testRunner = testRunner;
            }
        }
#endif
        controller->switchToEditorState();
        controller->startGameLoop();

        cout << testName << " succeded" << endl;

        return 0;
    }
    catch (const std::exception &exc)
    {
        cout << exc.what() << endl;
    }
    catch (...)
    {
        std::cerr << "Caught unknown exception." << std::endl;
    }

    return 1;
};
