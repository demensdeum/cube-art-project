#include "ScreenshotValidator.h"

#include <iostream>
#include <FlameSteelCore/FSCUtils.h>
#include <FlameSteelCommonTraits/Color.h>
#include <CubeArtProject/Utils/Factory.h>

#if CUBE_ART_PROJECT_ANDROID_NDK_BUILD
#include <android/log.h>
#endif

using namespace std;
using namespace FlameSteelCore::Utils;
using namespace FlameSteelEngine::CommonTraits;

ScreenshotValidator::ScreenshotValidator(shared_ptr<Screenshot> perfectScreenshot) {
    if (perfectScreenshot.get() == nullptr) {

#if CUBE_ART_PROJECT_ANDROID_NDK_BUILD
        __android_log_print(ANDROID_LOG_VERBOSE, "Cube Art Project Screenshot Validator", "Can't initialize ScreenshotValidator - perfect screenshot is null");
#endif

        throwRuntimeException(string("Can't initialize ScreenshotValidator - perfect screenshot is null"));
    }

    this->perfectScreenshot = perfectScreenshot;
}

bool ScreenshotValidator::isValid(shared_ptr<Screenshot> screenshot) {

    if (screenshot.get() == nullptr) {

        cout << "ScrenshotValidator - screenshot is empty so fail" << endl;

#if CUBE_ART_PROJECT_ANDROID_NDK_BUILD
        __android_log_print(ANDROID_LOG_VERBOSE, "Cube Art Project Screenshot Validator", "ScrenshotValidator - screenshot is empty so fail");
#endif

        return false;
    }

    auto screenshotWidth = screenshot->getWidth();
    auto screenshotHeight = screenshot->getHeight();

    if (screenshotWidth != perfectScreenshot->getWidth()) {
        cout << "ScrenshotValidator - screenshot have different width? so fail" << endl;

#if CUBE_ART_PROJECT_ANDROID_NDK_BUILD
        __android_log_print(ANDROID_LOG_VERBOSE, "Cube Art Project Screenshot Validator", "ScrenshotValidator - screenshot have different width? so fail");
#endif

        return false;
    }

    if (screenshotHeight != perfectScreenshot->getHeight()) {
        cout << "ScrenshotValidator - screenshot have different width? so fail" << endl;

#if CUBE_ART_PROJECT_ANDROID_NDK_BUILD
        __android_log_print(ANDROID_LOG_VERBOSE, "Cube Art Project Screenshot Validator", "ScrenshotValidator - screenshot have different height? so fail");
#endif

        return false;
    }

    cout << "screenshotWidth: " << screenshotWidth << " ; screenshotHeight: " << screenshotHeight << endl;

    for (auto y = 0; y < screenshot->getHeight() -1; y++) {
        for (auto x = 0; x < screenshot->getWidth() -1; x++) {
            auto lhs = screenshot->getColorAtXY(x,y);
            auto rhs = perfectScreenshot->getColorAtXY(x,y);
            if (lhs->isEqualTo(rhs) == false) {
                cout << "ScreenshotValidator - screenshots are not equal at x" << x << " y " << y << endl;
                cout << "screenshot pixel: "<< lhs->asString()  << endl;
                cout << "perfect screenshot pixel: "<< rhs->asString()  << endl;

#if CUBE_ART_PROJECT_ANDROID_NDK_BUILD
                __android_log_print(ANDROID_LOG_VERBOSE, "Cube Art Project Screenshot Validator", "ScreenshotValidator - screenshots are not equal");
#endif

                return false;
            }
        }
    }

#if CUBE_ART_PROJECT_ANDROID_NDK_BUILD
    __android_log_print(ANDROID_LOG_VERBOSE, "Cube Art Project Screenshot Validator", "ScrenshotValidator - screenshot checks passed");
#endif

    cout << "ScrenshotValidator - screenshot checks passed" << endl;

    return true;

};
