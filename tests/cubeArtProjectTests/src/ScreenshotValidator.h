#ifndef EMPTYSCENEINITIALIZETESTSCREENSHOTVALIDATOR_H_
#define EMPTYSCENEINITIALIZETESTSCREENSHOTVALIDATOR_H_

#include <FlameSteelCommonTraits/Screenshot.h>
#include <memory>

using namespace std;
using namespace FlameSteelEngine::CommonTraits;

class ScreenshotValidator {

public:
    ScreenshotValidator(shared_ptr<Screenshot> perfectScreenshot);
    bool isValid(shared_ptr<Screenshot> screenshot);

private:
    shared_ptr<Screenshot> perfectScreenshot;
};

#endif