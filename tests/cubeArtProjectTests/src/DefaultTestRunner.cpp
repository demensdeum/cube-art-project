#include "DefaultTestRunner.h"
#include <iostream>
#include "ScreenshotValidator.h"
#include <CubeArtProject/CubeArtProject.h>
#include <FlameSteelCommonTraits/Path.h>
#include <FlameSteelCore/Unused.h>

using namespace std;
using namespace CubeArtProjectTests;
using namespace FlameSteelCommonTraits;

void DefaultTestRunner::step() {

    unused(FSGL_OBJECT_ID);

    switch (iteration) {

    case 0:
    {
        inputController->wheelDiffY = 1;
    }
    break;

    case 1:
    {
        auto screenshot = mainGameController->takeScreenshot();
        auto perfectScreenshotPath = make_shared<Path>("com.cubeartproject.screenshots.perfect.graphics");
        auto perfectScreenshot = Factory::screenshotFromPath(perfectScreenshotPath);
        auto screenshotValidator = make_shared<ScreenshotValidator>(screenshot);
        if (screenshotValidator->isValid(screenshot) == false) {
            cout << " failed - empty scene screenshot is invalid - error 4" << endl;
            exit(1);
        }
    }
    break;

    case 2:
        exit(0);

    default:
        break;
    }

    iteration++;
};
