import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

capabilities = DesiredCapabilities.CHROME
capabilities['goog:loggingPrefs'] = { 'browser':'ALL' }
driver = webdriver.Chrome()
driver.get("http://localhost/tests/cubeArtProjectTests/indexFullscreen.html")

time.sleep(6)

exitCode = 1

for entry in driver.get_log('browser'):
    if entry["source"] == "console-api":
        message = entry["message"]
        if "ScrenshotValidator - screenshot checks passed" in message:
            print("Cube build remove test succeded")
            exitCode = 0

driver.close()

if exitCode != 0:
    print("Cube build remove test failed")
    
exit(exitCode)
