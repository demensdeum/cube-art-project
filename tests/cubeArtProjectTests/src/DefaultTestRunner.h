#ifndef CUBE_ART_PROJECT_TESTSDEFAULTTESTRUNNER_H_
#define CUBE_ART_PROJECT_TESTSDEFAULTTESTRUNNER_H_

#include <FlameSteelEngineGameToolkit/IO/Input/FSEGTInputController.h>
#include <CubeArtProject/MainGameController/MainGameController.h>
#include <CubeArtProject/Utils/TestRunner/TestRunner.h>
#include <FlameSteelCore/SharedNotNullPointer.h>

using namespace CubeArtProject;

namespace CubeArtProjectTests {

class DefaultTestRunner final: public TestRunner {

public:
    void step();

    NotNull<InputController> inputController;
    NotNull<CubeArtProject::MainGameController> mainGameController;

private:
    int iteration = 0;

};
};

#endif
