 
#Resources  
Resources must be accessed without any parent directory, all resources must be named in unique reverse doman identifier style. For example file screenshot.png: "com.demensdeum.cube-art-project.screenshot.graphics"
Last component of rDNS identifier must contain type of resource (.graphics, .music, .sound)