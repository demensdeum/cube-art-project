#ifndef CUBEARTPROJECTEDITORCONTROLLER_H_
#define CUBEARTPROJECTEDITORCONTROLLER_H_

#include <vector>
#include <memory>
#include <CubeArtProject/Models/Field3D.h>
#include <FlameSteelEngineGameToolkit/Controllers/GameController.h>
#include <CubeArtProject/Utils/FreeCameraControlsController/FreeCameraControlsController.h>
#include <CubeArtProject/Utils/TestRunner/TestRunner.h>
#include <CubeArtProject/Utils/SceneSerializer/SceneSerializer.h>

struct CursorColor {
    int red = 0;
    int green = 0;
    int blue = 0;
};

using namespace std;
using namespace CubeArtProject;

namespace CubeArtProject {

class EditorController: public GameController, public enable_shared_from_this<EditorController>, public FreeCameraControlsControllerDelegate {

public:
    virtual void step();
    virtual void freeCameraControlsControllerDidFinish(shared_ptr<FreeCameraControlsController> freeCameraControlsController);

    shared_ptr<TestRunner> testRunner;
    void removeOrAddCubeAt(int x, int y, int z, int redColor, int greenColor, int blueColor);

    shared_ptr<string> startedSerializedScene;

private:

    bool useKeyWasPressed = false;

    NotNull<SceneSerializer> sceneSerializer;
    NotNull<Object> cursorCube;
    CursorColor currentCursorColor;
    int currentCursorIndex = 0;

    shared_ptr<Field3D> field3D = make_shared<Field3D>();
    NotNull<FreeCameraControlsController> freeCameraControls;
    NotNull<Object> camera;
    NotNull<FreeCameraControlsController> freeCameraControlsController;
    NotNull<InputController> inputController;

    vector<CursorColor> cursorColors;

    bool isInitialized = false;

    shared_ptr<FSEGTTouch> touch;
};
};


#endif