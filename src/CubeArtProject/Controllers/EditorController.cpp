#include "EditorController.h"
#include <iostream>
#include <FlameSteelCore/FSCUtils.h>
#include <CubeArtProject/Utils/CubeBuilder.h>
#include <unistd.h>
#include <FlameSteelEngineGameToolkit/Utils/Factory.h>
#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>


using namespace CubeArtProject;
using namespace FlameSteelCore::Utils;

void EditorController::step() {

    if (isInitialized == false) {

        sceneSerializer = make<SceneSerializer>(toNotNull(objectsContext), toNotNull(shared_from_this()));

        cursorColors.push_back(
            CursorColor{255,255,255}
        );
        cursorColors.push_back(
            CursorColor{255,255,0}
        );
        cursorColors.push_back(
            CursorColor{0,255,255}
        );
        cursorColors.push_back(
            CursorColor{0,255,0}
        );
        cursorColors.push_back(
            CursorColor{255,0,255}
        );
        cursorColors.push_back(
            CursorColor{255,0,0}
        );
        cursorColors.push_back(
            CursorColor{0,0,255}
        );
        cursorColors.push_back(
            CursorColor{0,0,0}
        );

        currentCursorColor = cursorColors[0];

        camera = Factory::makeObject(
                     make_shared<string>("camera"),
                     make_shared<string>("camera"),
                     0,0,0,
                     1,1,1,
                     0,0,0
                 );
        objectsContext->addObject(camera.sharedPointer());

        freeCameraControlsController = make<FreeCameraControlsController>(camera, toNotNull(ioSystem->inputController), shared_from_this());
        inputController = toNotNull(ioSystem->inputController);

        auto cubeBuilder = make<CubeBuilder>();

        auto cursorRed = currentCursorColor.red;
        auto cursorGreen = currentCursorColor.green;
        auto cursorBlue = currentCursorColor.blue;

        cursorCube = toNotNull(cubeBuilder->makeCube(0, 0, 0, cursorRed, cursorGreen, cursorBlue));

        objectsContext->addObject(cursorCube.sharedPointer());

        if (startedSerializedScene.get() != nullptr) {
            sceneSerializer->deserialize(toNotNull(startedSerializedScene));
        }

        isInitialized = true;
    }

    if (testRunner.get() != nullptr) {
        testRunner->step();
    }

    auto cubeCursorPosition = freeCameraControlsController->cubeCursorPositionVector();
    auto cubePositionVector = FSEGTUtils::getObjectPosition(cursorCube.sharedPointer());
    cubePositionVector->x = cubeCursorPosition->x;
    cubePositionVector->y = cubeCursorPosition->y;
    cubePositionVector->z = cubeCursorPosition->z;

    if (inputController->wheelDiffY > 0) {
        cout << "Change color forward" << endl;
        auto surfaceMaterial = FSEGTUtils::getObjectSurfaceMaterial(cursorCube.sharedPointer());

        auto surface = surfaceMaterial->material->surface;

        currentCursorIndex += 1;
        if (currentCursorIndex >= cursorColors.size()) {
            currentCursorIndex = 0;
        }
        currentCursorColor = cursorColors[currentCursorIndex];

        auto cursorRed = currentCursorColor.red;
        auto cursorGreen = currentCursorColor.green;
        auto cursorBlue = currentCursorColor.blue;

        SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, cursorRed, cursorGreen, cursorBlue));
        surfaceMaterial->material->needsUpdate = true;
    }
    else if (inputController->wheelDiffY < 0) {
        cout << "Change color backward" << endl;
        auto surfaceMaterial = FSEGTUtils::getObjectSurfaceMaterial(cursorCube.sharedPointer());

        auto surface = surfaceMaterial->material->surface;

        currentCursorIndex -= 1;
        if (currentCursorIndex <= 0) {
            currentCursorIndex = cursorColors.size() - 1;
        }
        currentCursorColor = cursorColors[currentCursorIndex];

        auto cursorRed = currentCursorColor.red;
        auto cursorGreen = currentCursorColor.green;
        auto cursorBlue = currentCursorColor.blue;

        SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, cursorRed, cursorGreen, cursorBlue));
        surfaceMaterial->material->needsUpdate = true;
    }

    objectsContext->updateObject(cursorCube.sharedPointer());

    //cout << "EditorController step" << endl;

    inputController->pollKey();

    if (ioSystem->inputController.get() == nullptr) {
        throwRuntimeException(string("ioSystem->inputController.get() == nullptr"));
    }

    if (ioSystem->inputController->touches->size() > 0) {

        auto rawTouch = ioSystem->inputController->touches->objectAtIndex(0);
        auto nowTouch = static_pointer_cast<FSEGTTouch>(rawTouch);
        cout << "nowTouch:" << endl;
        cout << nowTouch->x << endl;
        cout << nowTouch->y << endl;

        if (touch.get() == nullptr && nowTouch->x >= 480 && nowTouch->y >= 160 && nowTouch->x <= 645 && nowTouch->y <= 309) {
            touch = nowTouch;
            removeOrAddCubeAt(
                cubePositionVector->x,
                cubePositionVector->y,
                cubePositionVector->z,
                currentCursorColor.red,
                currentCursorColor.green,
                currentCursorColor.blue
            );


            cout << "Change color forward" << endl;
            auto surfaceMaterial = FSEGTUtils::getObjectSurfaceMaterial(cursorCube.sharedPointer());

            auto surface = surfaceMaterial->material->surface;

            currentCursorIndex += 1;
            if (currentCursorIndex >= cursorColors.size()) {
                currentCursorIndex = 0;
            }
            currentCursorColor = cursorColors[currentCursorIndex];

            auto cursorRed = currentCursorColor.red;
            auto cursorGreen = currentCursorColor.green;
            auto cursorBlue = currentCursorColor.blue;

            SDL_FillRect(surface, NULL, SDL_MapRGB(surface->format, cursorRed, cursorGreen, cursorBlue));
            surfaceMaterial->material->needsUpdate = true;
        }
    }
    else if (ioSystem->inputController->useKeyPressed == true || ioSystem->inputController->shootKeyPressed == true) {
        cout << "Use key pressed" << endl;

        useKeyWasPressed = true;
    }

    if (useKeyWasPressed && ioSystem->inputController->useKeyPressed == false && ioSystem->inputController->shootKeyPressed == false) {
        removeOrAddCubeAt(
            cubePositionVector->x,
            cubePositionVector->y,
            cubePositionVector->z,
            currentCursorColor.red,
            currentCursorColor.green,
            currentCursorColor.blue
        );

        sceneSerializer->serialize();
        useKeyWasPressed = false;
    }

    if (ioSystem->inputController->touches->size() < 1) {
        touch = shared_ptr<FSEGTTouch>();
    }

    if (renderer.get() == nullptr) {
        throwRuntimeException(string("renderer == nullptr"));
    }
    renderer->render(gameData);

    freeCameraControlsController->step();
};

void EditorController::freeCameraControlsControllerDidFinish(shared_ptr<FreeCameraControlsController> freeCameraControlsController) {
    objectsContext->updateObject(camera.sharedPointer());
};

void EditorController::removeOrAddCubeAt(int x, int y, int z, int redColor, int greenColor, int blueColor) {

    cout << "removeOrAddCubeAtFront()" << endl;

    if (objectsContext.get() == nullptr) {
        throwRuntimeException(string("objectsContext is null"));
    }

    cout << "EditorController::removeOrAddCubeAtFront()" << endl;

    auto cube = field3D->cubeAtXYZ(x, y, z);

    if (cube.get() != nullptr) {
        field3D->removeCubeAtXYZ(x, y, z);
        objectsContext->removeObject(cube);
        cout << "objectsContext->removeObject(cube);" << endl;
    }
    else {
        auto cubeBuilder = make_shared<CubeBuilder>();

        auto cursorRed = redColor;
        auto cursorGreen = greenColor;
        auto cursorBlue = blueColor;

        auto cube = cubeBuilder->makeCube(x, y, z, cursorRed, cursorGreen, cursorBlue);
        field3D->addCubeAtXYZ(cube, x, y, z);

        FSEGTUtils::getObjectPosition(cube)->x = x;
        FSEGTUtils::getObjectPosition(cube)->y = y;
        FSEGTUtils::getObjectPosition(cube)->z = z;

        objectsContext->addObject(cube);
        cout << "objectsContext->addObject(cube);" << endl;
    }
};
