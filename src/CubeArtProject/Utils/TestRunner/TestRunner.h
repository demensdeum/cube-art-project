#ifndef CUBEARTPROJECT_TESTRUNNER_H_
#define CUBEARTPROJECT_TESTRUNNER_H_

namespace CubeArtProject {

class TestRunner {

public:
    virtual void step() = 0;

};

};


#endif