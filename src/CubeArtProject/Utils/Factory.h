#ifndef CUBEARTPROJECTFACTORY_H_
#define CUBEARTPROJECTFACTORY_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <FlameSteelCommonTraits/Path.h>
#include <FlameSteelCommonTraits/Screenshot.h>

using namespace FlameSteelCommonTraits;

namespace CubeArtProject {

class Factory {

public:
    static shared_ptr<Image> imageFromPath(shared_ptr<Path> path);
    static shared_ptr<Screenshot> screenshotFromPath(shared_ptr<Path> path);

private:
    static uint32_t getPixel(SDL_Surface *surface, int x, int y);

};

};


#endif