#include "SceneSerializer.h"
#include <CubeArtProject/Controllers/EditorController.h>
#include <FlameSteelEngineGameToolkit/Utils/FSEGTUtils.h>
#include <iostream>
#include <string>
#include <json/json.hpp>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

using namespace std;
using namespace nlohmann;
using namespace CubeArtProject;

SceneSerializer::SceneSerializer(NotNull<ObjectsContext> objectsContext, NotNull<EditorController> editorController) {
    this->objectsContext = objectsContext;
    this->editorController = editorController;
};

NotNull<string> SceneSerializer::serialize() {

    auto serializedString = string("{\"cubes\": [");
    auto serializedObjectsCount = 0;

    for (int i = 0; i < objectsContext->size() - 1; i++) {
        auto object = objectsContext->objectAtIndex(i);
        auto instanceIdentifier = *object->getInstanceIdentifier();
        if (instanceIdentifier == "camera") {
            continue;
        }
        auto surfaceMaterial = FSEGTUtils::getObjectSurfaceMaterial(object);
        if (surfaceMaterial.get() == nullptr) {
            continue;
        }
        auto cubePositionVector = FSEGTUtils::getObjectPosition(object);
        if (cubePositionVector.get() == nullptr) {
            continue;
        }

        if (serializedObjectsCount > 0) {
            serializedString += string(",");
        }

        if ( surfaceMaterial->material.get() == nullptr) {
            continue;
        }

        auto surface = surfaceMaterial->material->surface;
        uint32_t pixel = SceneSerializer::getPixel(surface, 0, 0);

        uint8_t redColorByte = 0;
        uint8_t greenColorByte = 0;
        uint8_t blueColorByte = 0;

        SDL_GetRGB(pixel, surface->format, &redColorByte, &greenColorByte, &blueColorByte);

        if (surface == nullptr) {
            continue;
        }

        serializedString += "[";
        serializedString += to_string(int(cubePositionVector->x));
        serializedString += ",";
        serializedString += to_string(int(cubePositionVector->y));
        serializedString += ",";
        serializedString += to_string(int(cubePositionVector->z));
        serializedString += ",";
        serializedString += to_string(redColorByte);
        serializedString += ",";
        serializedString += to_string(greenColorByte);
        serializedString += ",";
        serializedString += to_string(blueColorByte);
        serializedString += "]";
        serializedObjectsCount += 1;
    }

    serializedString += string("]}");

    cout << "serializedString:" << serializedString << endl;

#if __EMSCRIPTEN__
    EM_ASM_(
    {
        var serializedSceneJSON = UTF8ToString($0);
        var outputLinkText = "https://demensdeum.com/games/CubeArtProjectWEB/?scene=" + serializedSceneJSON;
        document.getElementById("serializedSceneTextField").value = outputLinkText;
    }, serializedString.c_str());
#endif

    return make<string>(serializedString.c_str());
};

void SceneSerializer::deserialize(NotNull<string> serializedScene) {
    try {
    auto json = json::parse(serializedScene->c_str());
    cout << "Deserialize JSON: " << json << endl;

    auto cubesVector = json["cubes"];
    cout << "Deserialized cubes vector: " << cubesVector << endl;

    for (auto rawCube : cubesVector) {

        cout << "Raw cube: "<< rawCube << endl;

        if (rawCube.size() != 6) {
            cout << "rawCube.size() = " << rawCube.size();
            continue;
        }
        auto x = rawCube[0];
        auto y = rawCube[1];
        auto z = rawCube[2];
        auto red = rawCube[3];
        auto green = rawCube[4];
        auto blue = rawCube[5];

        editorController->removeOrAddCubeAt(x, y, z, red, green, blue);
    }
    }
    catch (const std::exception &exc)
    {
        cout << exc.what() << endl;
    }
    catch (...)
    {
        std::cerr << "Caught unknown exception." << std::endl;
    }    
};

uint32_t SceneSerializer::getPixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    /* p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
    switch(bpp)
    {
    case 1:
        return *p;
    case 2:
        return *(Uint16 *)p;
    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
    case 4:
        return *(Uint32 *)p;

    default:
        return 0;
    }
}
