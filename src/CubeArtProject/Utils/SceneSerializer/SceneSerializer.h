#ifndef CUBEARTPROJECTSCENESERIALIZER_H_
#define CUBEARTPROJECTSCENESERIALIZER_H_

#include <string>
#include <FlameSteelCore/SharedNotNullPointer.h>
#include <FlameSteelEngineGameToolkit/Controllers/ObjectsContext.h>
#include <SDL2/SDL.h>

namespace CubeArtProject {
class EditorController;
};

using namespace Shortcuts;
using namespace FlameSteelEngine::GameToolkit;
using namespace CubeArtProject;

namespace CubeArtProject {

class SceneSerializer {

public:
    SceneSerializer(NotNull<ObjectsContext> objectsContext, NotNull<EditorController> editorController);

    NotNull<string> serialize();
    void deserialize(NotNull<string> serializedScene);

private:
    NotNull<EditorController> editorController;
    NotNull<ObjectsContext> objectsContext;
    static uint32_t getPixel(SDL_Surface *surface, int x, int y);

};

};

#endif