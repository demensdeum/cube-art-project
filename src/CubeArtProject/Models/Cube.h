#ifndef LINUXSTATICBUILDCUBE_H_
#define LINUXSTATICBUILDCUBE_H_

#include <FlameSteelCore/SharedNotNullPointer.h>
#include <string>

using namespace std;
using namespace Shortcuts;

class Cube {
public:
    int redColor;
    int greenColor;
    int blueColor;
    int x;
    int y;
    int z;

    NotNull<string> serialize();
    shared_ptr<Cube> deserialize(NotNull<string> serializedCube);
};

#endif